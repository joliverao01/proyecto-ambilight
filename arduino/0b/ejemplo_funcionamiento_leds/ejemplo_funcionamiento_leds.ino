#include <FastLED.h>    // Include the library FastLed
#define LED_PIN     7   // Define the pin of the connection
#define NUM_LEDS    60  // Define the max number of leds
CRGB leds[NUM_LEDS];    // Calls CRGB, the library of FastLed and define to it the max number of leds
void setup() { 
  FastLED.addLeds<WS2812, LED_PIN, GRB>(leds, NUM_LEDS); // Calls FastLed an add some options for the leds
}

void loop() {
  for (int i = 0; i <= 60; i++) { // Starts a loop 
    leds[i] = CRGB ( 255, 0, 0); // Define leds with i to increase the value and change the color for x led
    FastLED.show(); // Actualize the state of the leds
    delay(100); // Puts a delay of 0,1 seconds by led
  }
  for (int i = 60; i >= 0; i--) { // Starts a loop 
    leds[i] = CRGB ( 0, 0, 255); // Define leds with i to increase the value and change the color for x led
    FastLED.show(); // Actualize the state of the leds
    delay(100); // Puts a delay of 0,1 seconds by led
  }
}
