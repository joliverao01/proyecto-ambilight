const int led = 13; // declare the pin 13 like a led
char valor = 0;     // we use char because we'll use letters and not numbers 

void setup() {
 Serial.begin(9600);    // sets the serial port with a speed of conucation by 9600bps
 pinMode(led,OUTPUT);   // configure the led 13 like a output
 digitalWrite(led,LOW); // starts with the led off
}

void loop() {
 if (Serial.available() > 0) { // open the serial port reading the incoming data or values
  valor = Serial.read();       // saves the value received by the serial port in the variable valor
  if (valor == 'E') {          // if the value received is equal to E do
   digitalWrite(led,HIGH);     // turn on the led of the pin 13
  }
  else if (valor == 'A') {     // if the value received is equal to A do
   digitalWrite(led,LOW);      // turn of the led of the pin 13
  }
  else {                      // if none of the caracters receiveds is equal to "E" or "A" do
   Serial.println("character not allowed");  // write in the console "character not allowed"
  }
 }
}
