#include <Adafruit_NeoPixel.h>  // Include the library Adafruit_NeoPixel
#define LED_PIN     7           // Define the pin of the connection
#define NUM_LEDS    60          // Define the max number of leds
#define NUM_DATA 182            // NUM_LED * 3 + 2
#define RECON_TIME 1000         // after x seconds idle time, send afk again.

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LED, PIN, NEO_GRB + NEO_KHZ800); // Defines the properties of Adafruit_Neopixel

uint8_t led_color[NUM_DATA]; // A type of unsigned integer of length 8 bits
int index = 0; // Starts an int variable

void setup() {
  strip.begin(); // Sets in the first led
  strip.show(); // Initialize all pixels to 'off'
  Serial.begin(115200); // sets the serial port with a speed of conucation by 115200bps
  Serial.print("ozy"); // Send ACK string to host
}

void loop() {
  if (Serial.available() > 0) { // open the serial port reading the incoming data or values
  led_color[index++] = (uint8_t)Serial.read(); // sets the number of leds with the values received by the serial port
   if (index >= NUM_DATA){ // starts a condition what says what if the value of the index is smaller than the value NUM_DATA do
      Serial.write('y'); // write by the serial an y
      index = 0; // sets the value of the index to 0
    
      if ((led_color[0] == 'o') && (led_color[1] == 'z')){ // starts a condition what says what if the value of led_color[0] is equal to "o" and the value of the led_color[1] is equal to z do
        for(int i=0; i<NUM_LED; i++){ // starts a loop            
          int led_index = i*3 + 2; // starts an int variable to calculate the number of the led * 3 + 2
          strip.setPixelColor(i, strip.Color(led_color[led_index], led_color[led_index+1], led_color[led_index+2])); // change the color of the leds using led_index         
        }           
        strip.show(); // update the leds                   
      }               
   }           
  }     
  else {           
    Serial.write('y'); // wirte by the serial an y
    index = 0; // sets the index to 0
  }
}
