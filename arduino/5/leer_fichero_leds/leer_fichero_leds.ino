#include <FastLED.h> // Include the library FastLed
#include <SD.h> // Include the library SD
#define LED_PIN     7   // Define the pin of the connection
#define NUM_LEDS    60  // Define the max number of leds
CRGB leds[NUM_LEDS];    // Calls CRGB, the library of FastLed and define to it the max number of 

void setup() {
  FastLED.addLeds<WS2812, LED_PIN, GRB>(leds, NUM_LEDS); // Calls FastLed an add some options for the leds
}

void loop() {
File fd = File.open("/home/smx2a/proyecto-ambilight/processing/9/cogiendo_color_mas_arduino/leds.txt", FILE_READ); // open and read the file in the url indicated 

if (File) { // starts a condition
  while (File.available()) {  // while arduino is reading the file
    int n = File.parseInt(); // eeturns the first valid (long) integer number from the serial buffer
    float r = File.parseFloat(); // returns the first valid floating point number from the Serial buffer
    float g = File.parseFloat(); // returns the first valid floating point number from the Serial buffer
    float b = File.parseFloat(); // returns the first valid floating point number from the Serial buffer
    
    Serial.println(n); // print the value of n to the serial port
    Serial.println(r, 1); // print the value of r and 1 to the serial port
    Serial.println(g, 1); // print the value of g and 1 to the serial port
    Serial.println(b, 1); // print the value of b and 1 to the serial port
  }
  
  File.close(); // stop reading the file
}
}
