#include <FastLED.h>    // Include the library FastLed
#define LED_PIN     7   // Define the pin of the connection
#define NUM_LEDS    60  // Define the max number of leds
#define SERIAL_FLUSH    // Serial buffer cleared on LED latch
#define CLEAR_ON_START  // LEDs are cleared on reset

CRGB leds[NUM_LEDS];    // Calls CRGB, the library of FastLed and define to it the max number of leds

void setup() {
  FastLED.addLeds<WS2812, LED_PIN, GRB>(leds, NUM_LEDS); // Calls FastLed an add some options for the leds
  Serial.begin(9600); // sets the serial port with a speed of comunication by 9600bps
  Serial.flush(); // flush the serial port
  Serial.begin(SerialSpeed); // sets the serial port with a speed of conucation by 9600bps
  Serial.print("Ada\n"); // Send ACK string to host
}

void loop() {
   int id, r, g, b; // starts an id, r, g, b variables with int
  
   if (Serial.available()>=4) { // If data is available to read,
     if(Serial.read() == 0xff){ // starts a condition
       id = Serial.read(); // read it and store it in val
       r = Serial.read(); // read it and store it in val
       g = Serial.read(); // read it and store it in val
       b = Serial.read(); // read it and store it in val
     }
     

    leds[id] = CRGB(r,g,b); // Sets the number and the colors of the leds
    FastLED.show(); // Actualize the state of the leds 

    Serial.println(id); // print the value of id by the serial port
    Serial.println(r); // print the value of r by the serial port
    Serial.println(g); // print the value of g by the serial port
    Serial.println(b); // print the value of b by the serial port
    Serial.println("-----------------------"); // print ----------------------- by the serial port
   }
}
