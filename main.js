/////////////////////////////////////////////////////////////////////////// BARRA DE PROGRESO //////////////////////////////////////////////////////////////////////////////////////////////
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  var winScroll = document.body.scrollTop || document.documentElement.scrollTop; // '||' quiere decir uno o otro
  var height = document.documentElement.scrollHeight - document.documentElement.clientHeight; //Resta el height del documento menos el height de el desplazamiento de la barra
  var scrolled = (winScroll / height) * 100; //divide las variables y multiplica el resultado por 100
  document.getElementById("barra-progreso").style.width = scrolled + "%"; //Aumenta el width de la barra de progeso a medida que baja la barra de navegación
  // console.log(Math.round(scrolled) + "%");
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function includeHTML() {
var z, i, elmnt, file, xhttp;
/*loop through a collection of all HTML elements:*/
z = document.getElementsByTagName("*");
for (i = 0; i < z.length; i++) {
  elmnt = z[i];
  /*search for elements with a certain atrribute:*/
  file = elmnt.getAttribute("snippet");
  if (file) {
    /*make an HTTP request using the attribute value as the file name:*/
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4) {
        if (this.status == 200) {elmnt.innerHTML = this.responseText;}
        if (this.status == 404) {elmnt.innerHTML = "Page not found.";}
        /*remove the attribute, and call this function once more:*/
        elmnt.removeAttribute("snippet");
        includeHTML();
      }
    }
    xhttp.open("GET", file, true);
    xhttp.send();
    /*exit the function:*/
    return;
  }
}
}

$( document ).ready(function() {

  $(window).scroll(function(event) {

    var valueScrollTop = $(window).scrollTop(); //Crea la variable que te dice los pixeles exactos en los que está la barra de navegación
    var windowWidth = $(window).width();

    if (windowWidth >= 1200) {
      if (valueScrollTop < 70) { //Si el valor de la barra de navegación es inferior a
        $('#contenedor-cabecera').css({'position': 'relative'});
        $('#cabecera').css({'height':'143px'});
        $('.entrada-menu').css({'margin-bottom': '0.5%', 'margin-top': '0.5%', 'margin-left': '2%'});
        $('h1, h2').css({'float': 'none', 'margin-left': 'none'})
        $('h1').css({'text-align': 'center', 'font-size': '400%', 'margin-top': '3%'})
        $('h2').css({'text-align': 'center', 'font-size': '200%', 'margin-top': 'none'})
        $('h3').css({'margin-top': '-2%', 'margin-right': '2%', 'text-align': 'right', 'float': 'none'})
      } else  if (valueScrollTop >= 150) { //Si el valor de la barra de navegación es mayor a 150px haz:
        $('#contenedor-cabecera').css({'position': 'sticky', 'top':'0'});
        $('#cabecera').css({'height':'70px'})
        $('.entrada-menu').css({'margin-bottom': 'none', 'margin-top': 'none', 'margin-left': '0.1%'});
        $('h1, h2').css({'text-align': 'center', 'float': 'left', 'margin-left': '1%'});
        $('h1').css({'font-size': '300%', 'margin-top': '1.2%'});
        $('h2').css({'font-size': '200%', 'margin-top': '1.8%'});
        $('h3').css({'font-size': '100%', 'margin-top': '2%', 'text-align': 'right', 'float': 'right'})
      }

    };

  });

  ////////////////////////////////////////////////////////////LAS ENTRADAS DEL MENU DE NAVEGACIÓN////////////////////////////////////////////////////////////////////////////////////
  $('#introduction-code').click(function() { $('html, body').animate({scrollTop: ($('#div-de-toda-la-pagina').offset().top)-200},200); });
  $('#target-code').click(function() { $('html, body').animate({scrollTop: ($('#titulo-objectius-del-treball').offset().top)-200},200); });
  $('#material-code').click(function() { $('html, body').animate({scrollTop: ($('#titulo-materials').offset().top)-200},200); });
  $('#economic-valuation-code').click(function() { $('html, body').animate({scrollTop: ($('#titulo-valoracio-economica-del-proyecte').offset().top)-200},200); });
  $('#glossary-code').click(function() { $('html, body').animate({scrollTop: ($('#titulo-glosario').offset().top)-200},200); });
  $('#arduino-code').click(function() { $('html, body').animate({scrollTop: ($('#titulo-arduino').offset().top)-200},200); });
  $('#processing-code').click(function() { $('html, body').animate({scrollTop: ($('#titulo-processing').offset().top)-200},200); });
  $('#mounting-code').click(function() { $('html, body').animate({scrollTop: ($('#titulo-mounting').offset().top)-200},200); });
  $('#gallery-code').click(function() { $('html, body').animate({scrollTop: ($('#titulo-gallery').offset().top)-200},200); });
  $('#next-code').click(function() { $('html, body').animate({scrollTop: ($('#titulo-next').offset().top)-200},200); });
  $('#result-code').click(function() { $('html, body').animate({scrollTop: ($('#titulo-resultado').offset().top)-200},200); });
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  var valueWindowWidth = $(window).width();

  if (valueWindowWidth <= 350) {
    $('#cabecera').css({'height': '100px'});
    $('h1').css({'font-size': '200%', 'text-align': 'left', 'margin-left': '2%'});
    $('h2').css({'font-size': '100%', 'text-align': 'left', 'margin-left': '6%'});
    $('h3').css({'font-size': '80%', 'text-align': 'right', 'margin-top': '-8.5%'});
    $('.entrada-menu').css({'font-size': '80%', 'margin-left': '1%'});
    $('#contenedor-texto-1').css({'margin-left': '2%', 'margin-right': '2%'});
    $('a, p, td, th').css({'font-size': '70%'});
    $('.texto-imagenes').css({'font-size': '180%'});
    $('#ambilight-footer').css({'width':'50%'});
    $('.card').css({'width':'600px'});
    $('.slideshow').css({'height':'400px'});
    $('.img-mounting').css({'width':'100%'});
    $('h12').css({'font-size': '100%'});
    $('h13').css({'font-size': '70%'});
    $('h14').css({'font-size': '80%'});
    $('.download-button').css({'font-size': '105%', 'width': '40%', 'height': '30px', 'margin-bottom': '1%', 'border-radius': '5%'});
    $('.select').css({'font-size': '60%', 'width': '100%'});
    $('#social').css({'width':'40%', 'border': '2px solid black'});
    $('.social').css({'width':'10%', 'margin-left': '0%', 'float': 'left', 'margin': '1%'});
    $('#meet-us').css({'font-size':'100%', 'margin-left': '-15%'});
    $('.meet-us').css({'width': '100%'});
    $('#twitter').css({'margin-left': '25%'});
  }

  else if (valueWindowWidth > 279 && valueWindowWidth <= 479) {
    $('#cabecera').css({'height': '100px'});
    $('h1').css({'font-size': '200%', 'text-align': 'left', 'margin-left': '5%'});
    $('h2').css({'font-size': '100%', 'text-align': 'left', 'margin-left': '10%'});
    $('h3').css({'font-size': '80%', 'text-align': 'right', 'margin-top': '-6.5%'});
    $('.entrada-menu').css({'font-size': '80%'});
    $('#contenedor-texto-1').css({'margin-left': '3%', 'margin-right': '3%'});
    $('a, p, td, th').css({'font-size': '70%'});
    $('.mounting-partes').css({'font-size': '100%'});
    $('.img-mounting').css({'width':'100%'});
    $('.letra-glossary').css({'font-size': '100%'});
    $('.texto-imagenes').css({'font-size': '180%'});
    $('#ambilight-footer').css({'width':'60%'});
    $('h12').css({'font-size': '150%'});
    $('h13').css({'font-size': '120%'});
    $('h14').css({'font-size': '100%'});
    $('.download-button').css({'font-size': '105%', 'width': '30%', 'height': '30px', 'margin-bottom': '1%', 'border-radius': '5%'});
    $('.select').css({'font-size': '60%', 'width': '100%'});
    $('#social').css({'width':'35%', 'border': '2px solid black'});
    $('.social').css({'width':'10%', 'margin-left': '5%'});
    $('#meet-us').css({'font-size':'100%', 'margin-left': '0%'});
    $('.meet-us').css({'width': '100%'});
    $('#twitter').css({'margin-left': '25%'});
  }

  else if (valueWindowWidth > 479 && valueWindowWidth <= 767) {
    $('#cabecera').css({'height': '100px'});
    $('h1').css({'font-size': '300%', 'text-align': 'left', 'margin-left': '2%', 'margin-top': '3%'});
    $('h2').css({'font-size': '200%', 'text-align': 'left', 'margin-left': '6%'});
    $('h3').css({'font-size': '150%', 'text-align': 'right', 'margin-top': '-6.5%'});
    $('.entrada-menu').css({'font-size': '100%'});
    $('#contenedor-texto-1').css({'margin-left': '3%', 'margin-right': '3%'});
    $('.card').css({'width':'120%'});
    $('.texto-imagenes').css({'font-size': '180%'});
    $('.img-mounting').css({'width':'100%'});
    $('#ambilight-footer').css({'width':'60%'});
    $('h12').css({'font-size': '200%'});
    $('h13').css({'font-size': '100%'});
    $('h14').css({'font-size': '100%'});
    $('.download-button').css({'font-size': '105%', 'width': '20%', 'height': '30px', 'margin-bottom': '1%', 'border-radius': '5%'});
    $('.select').css({'font-size': '60%', 'width': '100%'});
    $('#social').css({'width':'35%', 'border': '2px solid black'});
    $('.social').css({'width':'10%', 'margin-left': '0%', 'float': 'left', 'margin': '1%'});
    $('#meet-us').css({'font-size':'100%', 'margin-left': '-20%'});
    $('.meet-us').css({'width': '100%'});
    $('#twitter').css({'margin-left': '25%'});
  }

  else if (valueWindowWidth > 767 && valueWindowWidth <= 991) {
    $('#cabecera').css({'height': '140px'});
    $('h1').css({'font-size': '400%', 'text-align': 'left', 'margin-left': '15%', 'margin-top': '4%'});
    $('h2').css({'font-size': '250%', 'text-align': 'left', 'margin-left': '17%'});
    $('h3').css({'font-size': '150%', 'text-align': 'right', 'margin-top': '-5.5%'});
    $('.entrada-menu').css({'font-size': '120%'});
    $('#contenedor-texto-1').css({'margin-left': '3%', 'margin-right': '3%'});
    $('#ambilight-footer').css({'width':'60%'});
    $('h12').css({'font-size': '250%'});
    $('.img-mounting').css({'width':'100%'});
    $('h13').css({'font-size': '150%'});
    $('h14').css({'font-size': '150%'});
    $('#social').css({'width':'35%', 'border': '2px solid black'});
    $('.social').css({'width':'10%', 'margin-left': '0%', 'float': 'left', 'margin': '1%'});
    $('#meet-us').css({'font-size':'125%'});
    $('.meet-us').css({'width': '100%', 'margin-left': '0%'});
    $('#twitter').css({'margin-left': '25%'});
  }

  $(".mounting-parts").siblings().css("display", "none");

  $(".mounting-parts").click(function(){
    $(this).siblings().toggle("fast");
  });

  $(".toggle-code").click(function() {
    $("#1p-snippet").toggle("slow");
  });

  $(".materials").mouseenter(function(){
    $(this).css({"filter": "grayscale(100%)"});
    $(this).children().css({"color": "white"});
  });

  $(".card").mouseleave(function(){
    $(this).css({"filter": "grayscale(0%)"});
    $(this).children().css({"color": "transparent"});
  });

  $("#selectpde").change(function(){
      $('#download-link-processing').attr('href', $("#selectpde option:selected").attr("href"));
      $(".processing-snippet").hide(300);
      $("#"+$(this).val()+"-snippet").show(300);
  });

  $("#selectino").change(function(){
    $('#download-link-arduino').attr('href', $("#selectino option:selected").attr("href"));
    $(".arduino-snippet").hide(300);
    $("#"+$(this).val()+"-snippet").show(300);
  });

  for (var i = 1; i <= 36; i++) {
    $(".montaje"+i).css({'background':'url(img/matts/'+i+'.jpg', 'background-size': 'cover', 'z-index': '1'});
  }


});
