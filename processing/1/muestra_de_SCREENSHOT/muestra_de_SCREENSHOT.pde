import java.awt.*; // Include all the libraries of java.awt
PImage screenshot; // Type of data for save images

void setup() {
  size(500,500); // Size of the screen of processing
  //fullScreen(); // Go fullscreen
  frameRate(55000); // Specifies the framerate
}

void draw() {
  if (frameCount % 0.5 == 0) screenshot(); // Time between screenshots
  if (screenshot != null) image(screenshot, 0, 0, width, height); // Define the size of the screenshots
  println(frameRate); // Muestra el framerate 
}

void screenshot() {
  try {
    screenshot = new PImage(new Robot().createScreenCapture(new Rectangle(0, 0, displayWidth, displayHeight))); // Do and show the screenshots in a rectangle
  } catch (AWTException e) {} // That function tries to handle the exceptions
}
