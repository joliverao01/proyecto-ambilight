import java.awt.*; // Include all the libraries of java.awt
import processing.serial.*; // Import the library of processing.serial to send data by the serial

PImage screenshot; // Type of data to store screenshots

Serial myPort; // Create object from Serial class

int max_LED_X=21; // Define a int with the max number led horizontal
int max_LED_Y=11; // Define a int with the max number led vertical

int max_width; // Define an int to get the max width resolution of the user
int max_height; // Define an int to get the max height resolution of the user

int max_pixel_width; // Define an int to save the width of the rect
int max_pixel_height; // Define an int to save the height of the rect

static final color leds[][] = new color[12][22]; // Define a matrix with color
static final int led_id[][] = new int[12][22]; // Define a matrix with int

void mappear() { 
  for (int i = 0; i <= max_LED_X; i++) { // Starts a loop
    for (int j = 0; j <= max_LED_Y; j++) { // Starts a loop inside a loop
      led_id[j][i] = -1; // Define in the matrix all the numbers with 0 to -1
    }
  }
  // Bottom side
  led_id[11][1] = 0;
  led_id[11][2] = 1;
  led_id[11][3] = 2;
  led_id[11][4] = 3;
  led_id[11][5] = 4;
  led_id[11][6] = 5;
  led_id[11][7] = 6;
  led_id[11][8] = 7;
  led_id[11][9] = 8;
  led_id[11][10] = 9;
  led_id[11][12] = 10;
  led_id[11][12] = 11;
  led_id[11][13] = 12;
  led_id[11][14] = 13;
  led_id[11][15] = 14;
  led_id[11][16] = 15;
  led_id[11][17] = 16;
  led_id[11][18] = 17;
  led_id[11][19] = 18;
  led_id[11][20] = 19;
  //////////////////
  // Right side
  led_id[10][21] = 20;
  led_id[9][21] = 21;
  led_id[8][21] = 22;
  led_id[7][21] = 23;
  led_id[6][21] = 24;
  led_id[5][21] = 25;
  led_id[4][21] = 26;
  led_id[3][21] = 27;
  led_id[2][21] = 28;
  led_id[1][21] = 29;
  ///////////////////
  // Top side
  led_id[0][20] = 30;
  led_id[0][19] = 31;
  led_id[0][18] = 32;
  led_id[0][17] = 33;
  led_id[0][16] = 34;
  led_id[0][15] = 35;
  led_id[0][14] = 36;
  led_id[0][13] = 37;
  led_id[0][12] = 38;
  led_id[0][11] = 39;
  led_id[0][10] = 40;
  led_id[0][9] = 41;
  led_id[0][8] = 42;
  led_id[0][7] = 43;
  led_id[0][6] = 44;
  led_id[0][5] = 45;
  led_id[0][4] = 46;
  led_id[0][3] = 47;
  led_id[0][2] = 48;
  led_id[0][1] = 49;
  //////////////////
  // Left side
  led_id[1][0] = 50;
  led_id[2][0] = 51;
  led_id[3][0] = 52;
  led_id[4][0] = 53;
  led_id[5][0] = 54;
  led_id[6][0] = 55;
  led_id[7][0] = 56;
  led_id[8][0] = 57;
  led_id[9][0] = 58;
  led_id[10][0] = 59;
  ///////////////////

}

void getColorTopLine() {
  int top_x = 0;
  int top_y = 5;
  // LEDS Top Side
  for (int i = 1; i <= max_LED_X; i++) { // Starts a loop
    leds[0][i] = get(top_x, top_y); // Get the color of x pixel in x position
    top_x = top_x + max_pixel_width+3; // Actualize the variable top_x with new values
  }
}

void getColorBotLine() {
  int bot_x = 0;
  int bot_y = max_height - 35;
  // LEDS Bottom Side
  for (int i = 1; i <= max_LED_X; i++) { // Starts a loop
    leds[11][i] = get(bot_x, bot_y); // Get the color of x pixel in x position
    bot_x = bot_x + max_pixel_width+3; // Actualize the variable bot_x with new values
  }
}

void getColorLeftLine() {
  int left_x = 5;
  int left_y = 0;
  // LEDS Left Side
  for (int i = 1; i <= max_LED_Y; i++) { // Starts a loop
    leds[i][0] = get(left_x, left_y); // Get the color of x pixel in x position
    left_y = left_y + max_pixel_height+5; // Actualize the variable left_y with new values
  }
}

void getColorRightLine() {
  int right_x = max_width - 5;
  int right_y = 0;
  // LEDS Right Side
  for (int i = 1; i <= max_LED_Y; i++) { // Starts a loop
    leds[i][21] = get(right_x, right_y); // Get the color of x pixel in x position
    right_y = right_y + max_pixel_height+5; // Actualize the variable right_y with new values
  }
}

void printMatrix() {
   for (int i=0; i<leds.length; i++) { // Starts a loop
     for (int j=0; j<leds[0].length; j++) { // Starts a loop inside a loop
       print(leds[i][j]+" "); // Print the position of the leds
     }
     print("\n"); // Linebreak
   } 
}

void printMap() {
   for (int i=0; i<leds.length; i++) { // Starts a loop
     for (int j=0; j<leds[0].length; j++) { // Starts a loop inside a loop
       print(led_id[i][j]+" ");   // Sows the values of led_id
     }
     print("\n"); // Linebreak
   } 
}

void printRGB() {
   for (int i=0; i<leds.length; i++) { // Starts a loop
     for (int j=0; j<leds[0].length; j++) { // Starts a loop inside a loop
          color color_pixel = leds[i][j]; // Save the color of the leds
          int r,g,b; // Starts the variables r,g,b
          r = (int) red(color_pixel); // Define r in the color_pixel
          g = (int) green(color_pixel); // Define g in the color_pixel
          b = (int) blue(color_pixel); // Define b in the color_pixel
          print("("+r+","+g+","+b+")"); // Shows the rgb values recorded
     }
     print("\n"); // Linebreak
   } 
}

void sendMatrixByToken() {
  for (int i=0; i<led_id.length; i++) { // Starts a loop
     for (int j=0; j<led_id[0].length; j++) { // Starts a loop inside a loop
       if (led_id[i][j] != -1) { // Starts a condition
          color color_pixel = leds[i][j]; // Save the color of the leds
          int r,g,b; // Starts the variables r,g,b
          r = (int) red(color_pixel); // Define r in the color_pixel
          g = (int) green(color_pixel); // Define g in the color_pixel
          b = (int) blue(color_pixel); // Define b in the color_pixel
          
          myPort.write(led_id[i][j]+","+r+","+g+","+b+"\n"); // Sends the values of led_id, r, g and b
          
          print(led_id[i][j]+","+r+","+g+","+b+"\n"); // Prints the values sended
       }
     }
   } 
}

void sendMatrix() {

  for (int i=0; i<led_id.length; i++) { // Starts a loop
     for (int j=0; j<led_id[0].length; j++) { // Starts a loop inside a loop
       if (led_id[i][j] != -1) { // Starts a condition
          color color_pixel = leds[i][j]; // Save the color of the leds
          int r,g,b; // Starts the variables r,g,b
          r = (int) red(color_pixel); // Define r in the color_pixel
          g = (int) green(color_pixel); // Define g in the color_pixel
          b = (int) blue(color_pixel); // Define b in the color_pixel
          myPort.write(led_id[i][j]);
          myPort.write(r); // Sends the value of r
          myPort.write(g); // Sends the value of g
          myPort.write(b); // Sends the value of b
       }
     }
   } 
}

void setup() {
  // GET THE RESOLUTION OF THE USER
  GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice(); // To start to capture the resolution of the user
  max_width = gd.getDisplayMode().getWidth(); // Captures the max width and save it in max_width
  max_height = gd.getDisplayMode().getHeight(); // Captures the max height and save it in max_height
  //size(500, 500); // Size of the screen
  max_pixel_width  = max_width/22; // Divide the max width of the screen by the max horizontal number of leds + 2
  max_pixel_height = max_height/12; // Divide the max height of the screen by the max height number of leds + 2
  fullScreen(); // Sets the screen to fullscreen
  myPort = new Serial(this, "/dev/ttyUSB0", 9600); // Define the serial port on arduino is connected
  myPort.bufferUntil('\n');  // Do a linebreak until the buffer
};

void draw() {
  if (frameCount % 0.5 == 0) screenshot(); // Time between screenshots
  if (screenshot != null) image(screenshot, 0, 0, width, height); // Define the size of the screenshots
  getColorTopLine(); // Calls the void to execute it continually
  getColorBotLine(); // Calls the void to execute it continually
  getColorLeftLine(); // Calls the void to execute it continually
  getColorRightLine(); // Calls the void to execute it continually
  //printMatrix(); // Calls the void to execute it continually
  printRGB(); // Calls the void to execute it continually
  mappear(); // Calls the void to execute it continually
  printMap(); // Calls the void to execute it continually
  sendMatrixByToken(); // Calls the void to execute it continually
}

void screenshot() {
  try {
    screenshot = new PImage(new Robot().createScreenCapture(new Rectangle(0, 0, displayWidth, displayHeight))); // Do and show the screenshots in a rectangle
  } catch (AWTException e) {} // That function tries to handle the exceptions
}
