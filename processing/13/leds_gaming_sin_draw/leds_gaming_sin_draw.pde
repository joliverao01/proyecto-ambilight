import java.awt.*; // Include all the libraries of java.awt
import processing.serial.*; // Import the library of processing.serial to send data by the serial

static int N_LEDS = 60; // Define the number of the max leds

PImage screenshot; // Type of data to store screenshots

static final color leds[] = new color[N_LEDS]; // Define a vector with color

int max_width; // Define an int to get the max width resolution of the user
int max_height; // Define an int to get the max height resolution of the user

int max_pixel_width; // Define an int to save the width of the rect
int max_pixel_height; // Define an int to save the height of the rect

void getColorTopLine() {
  int top_x = 0;
  int top_y = 5;
  // LEDS Top Side
  for (int i = 30; i <= 49; i++) { // Starts a loop
    leds[i] = get(top_x, top_y); // Get the color of x pixel in x position
    top_x = top_x + max_pixel_width+3; // Actualize the variable top_x with new values
  }
}

void getColorBottomLine() {
  int bot_x = 0;
  int bot_y = max_height - 35;
  // LEDS Bottom Side
  for (int i = 0; i <= 19; i++) { // Starts a loop
    leds[i] = get(bot_x, bot_y); // Get the color of x pixel in x position
    bot_x = bot_x + max_pixel_width+3; // Actualize the variable bot_x with new values
  }
}

void getColorLeftLine() {
  int left_x = 5;
  int left_y = 0;
  // LEDS Left Side
  for (int i = 50; i <= 59; i++) { // Starts a loop
    leds[i] = get(left_x, left_y); // Get the color of x pixel in x position
    left_y = left_y + max_pixel_height+5; // Actualize the variable left_y with new values
  }
}

void getColorRightLine() {
  int right_x = max_width - 5;
  int right_y = 0;
  // LEDS Right Side
  for (int i = 20; i <= 29; i++) { // Starts a loop
    leds[i] = get(right_x, right_y); // Get the color of x pixel in x position
    right_y = right_y + max_pixel_height+5; // Actualize the variable right_y with new values
  }
}

void updateLedState() {
  takeScreenshoots(); // Calls the void to execute it continually
  getColorTopLine(); // Calls the void to execute it continually
  getColorBottomLine(); // Calls the void to execute it continually
  getColorLeftLine(); // Calls the void to execute it continually
  getColorRightLine(); // Calls the void to execute it continually
}

void takeScreenshoots()  {
  if (frameCount % 0.5 == 0) screenshot(); // Time between screenshots
  if (screenshot != null) image(screenshot, 0, 0, width, height); // Define the size of the screenshots
}


void setup() {
  // GET THE RESOLUTION OF THE USER
  GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice(); // To start to capture the resolution of the user
  max_width = gd.getDisplayMode().getWidth(); // Captures the max width and save it in max_width
  max_height = gd.getDisplayMode().getHeight(); // Captures the max height and save it in max_height
  //size(500, 500); // Size of the screen
  max_pixel_width  = max_width/22; // Divide the max width of the screen by the max horizontal number of leds + 2
  max_pixel_height = max_height/12; // Divide the max height of the screen by the max height number of leds + 2
  fullScreen(); // Sets the screen to fullscreen
  
  byte[] buffer = new byte[6 + N_LEDS * 3]; // Starts a vector of byte to define the size of the buffer
  Serial myPort; // Create object from Serial class
  int  r, g, b, t, prev, frame = 0; // Starts variables and define frame to 0
  long totalBytesSent = 0; // Define a long, what is a datatype for large integers
  
  myPort = new Serial(this, "/dev/ttyUSB0", 115200); // Define the serial port on arduino is connected

  buffer[0] = 'A';                                // Magic word 1
  buffer[1] = 'd';                                // Magic word 2
  buffer[2] = 'a';                                // Magic word 3
  buffer[3] = byte((N_LEDS - 1) >> 8);            // LED count high byte
  buffer[4] = byte((N_LEDS - 1) & 0xff);          // LED count low byte
  buffer[5] = byte(buffer[3] ^ buffer[4] ^ 0x55); // Checksum

  prev  = second(); // For bandwidth statistics


  for (;;) {
    
    updateLedState(); // Calls the void to execute it continually

     int k = 0;
    // Start at position 6, after the LED header/magic word
    for (int i = 6; i < buffer.length; ) {
        
      color color_pixel = leds[k];
      r = (int) red(color_pixel); // Translates the r float variable to an int variable for the integer
      g = (int) green(color_pixel); // Translates the r float variable to an int variable for the integer
      b = (int) blue(color_pixel); // Translates the r float variable to an int variable for the integer
      
      buffer[i++] = byte(r); // Loads in the buffer the value of r
      buffer[i++] = byte(g); // Loads in the buffer the value of g
      buffer[i++] = byte(b); // Loads in the buffer the value of b
      
      print("["+k+"]"+"("+r+","+g+","+b+")"); // Print the number of the led and the RGB values
      
      k++; // Increase the value of k
    }
    
    print("\n"); // Line break
    myPort.write(buffer); // Sends the data stored on the buffer by the serial port
    totalBytesSent += buffer.length; // Addition totalBytesSent + the total length of the buffer
    frame++; // Increase the value of frame
    
    /*
    // Update statistics once per second
    if ((t = second()) != prev) {
      print("Average frames/sec: "); // Prints the framerate
      print(int((float)frame / (float)millis() * 1000.0));
      print(", bytes/sec: "); 
      println(int((float)totalBytesSent / (float)millis() * 1000.0));
      prev = t;
    }
    */
  }
}

void draw() { 

}

void screenshot() {
  try {
    screenshot = new PImage(new Robot().createScreenCapture(new Rectangle(0, 0, displayWidth, displayHeight))); // Do and show the screenshots in a rectangle
  } catch (AWTException e) {} // That function tries to handle the exceptions
}
