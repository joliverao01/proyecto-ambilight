void setup() {
  colorMode(RGB, 255, 255, 255); // Sets the mode of color to RGB
  size(500, 500); // Define the size of the screen 
  for(int x = 0; x < width; x++) { // Starts a loop
   for(int y = 0; y < height; y++) { // Starts a loop inside other loop
      float r = map(noise(x / 80.0, y / 80.0, 10.0), 0, 1, 0, 255); // Defines rgb to r
      float g = map(noise(x / 80.0, y / 90.0, 20.0), 0, 1, 0, 255); // Defines rgb to g
      float b = map(noise(x / 80.0, y / 80.0, 30.0), 0, 1, 0, 255); // Defines rgb to b
      stroke(r,g,b); // Sets the color used to draw lines and borders around shapes
      point(x, y); //    Draws a point, a coordinate in space at the dimension of one pixel
    }
  }
}

void draw() {
  color of_the_screen = get(100,25); { // Get the color of 100,25 pixel position 
  fill(of_the_screen); // Refill with the color captured in the pixel
  rect(0, 200, width, 100); // Creates a rect
  }
  
  color color_under_mouse = get(mouseX, mouseY);{ // Get the color of the pixel under the mouse
  fill(color_under_mouse); // Refill with the color captured 
  rect(0, 100, width, 100); // Draw a rect
  }
  
  print("\n"); // Line break
  print(color_under_mouse); // Shows the rgb color captured by color_under_mouse
    
  float r,g,b; // Creates r,g,b float variables
  
  r = red(color_under_mouse); // Get the red value of the color captured
  g = green(color_under_mouse); // Get the green value of the color captured
  b = blue(color_under_mouse ); // Get the blue value of the color captured
  
  print("\n"); // Line break
  print(r); // Shows the red value
  print("\n"); // Line break
  print(g); // Shows the green value
  print("\n"); // Line break
  print(b); // Shows the blue value
  print("\n"); // Line break
}
