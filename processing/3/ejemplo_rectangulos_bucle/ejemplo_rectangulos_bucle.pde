void setup() { 
  size(500, 500); // Define the size of the screen
  gameBoard(); // Calls gameBoard on start
 }

void gameBoard() {
  float x = width*.10; // Creates a variable
  float y = height*.10; // Creates a variable
  rect(width*.10,height*.10,width*.80,height*.80); // Creates a variable

  // Left and top zones
  for (int i = 1; i < 9; i++) { // Creates a loop
    for (int j = 1; j < 9; j++) { // Creates a loop
    rect(x*i,y,x,y); // Creates a rect
    rect(x,y*j,x,y); // Creates a rect
    }
  }
  
  // Right and bottom zones
  for ( int i = 1; i < 9; i++) { // Creates a loop 
    for( int j = 1; j< 9; j++) { // Creates a loop
    rect(x*i,y*8,x,y); // Creates a rect
    rect(x*8,y*i,x,y); // Creates a rect
    }
  }
}
