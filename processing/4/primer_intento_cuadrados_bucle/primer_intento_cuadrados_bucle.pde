void setup() {
  //size(500, 500); // Define the size of the screen
  fullScreen(); // Define the screen to fullScreen
}

void draw() {

  float cell_width = width/60; // Number of leds/horizontal rects
  float cell_height = height/30; // Number of leds/vertical rects

  int number = 0;  // Creates and declare an int variable
  
  float x; // Declare float variable 
  float y; // Declare float variable
  
  ///////////////////////////////////////////////////////////// top /////////////////////////////////////////////////////////////  
  x = 0;
  y = 0;
  
  for (int i=0; i <= 60; i++) { // Creates a loop
    rect(x, y, cell_width, cell_height); // Create a rect
    fill(0); // Fill the text with color
    text(number++, x + cell_width/3, y + cell_height/2); // Put the identificator number
    fill(0, 102, 153, 204); // Fill the rect with color
    
    x = x + cell_width;
    
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  
  ////////////////////////////////////////////////////////////// right //////////////////////////////////////////////////////////
  x = width-cell_width;;
  y = 0;
  number = number -2;
  
  for (int i=0; i <= 30; i++) { // Creates a loop
    rect(x, y, cell_width, cell_height); // Create a rect
    fill(0); // Fill the text with color
    text(number++, x + cell_width/3, y + cell_height/2); // Put the identificator number
    fill(0, 102, 153, 204); // Fill the rect with color
    y = y + cell_height;
  }
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  number = number + 57;
  
  ////////////////////////////////////////////////////////////// bottom //////////////////////////////////////////////////////////
  x = 0;
  y = height-cell_height;

  for (int i=0; i <= 60; i++) { // Creates a loop
    rect(x, y, cell_width, cell_height); // Create a rect 
    fill(0); // Fill the text with color
    text(number--, x + cell_width/4, y + cell_height/2); // Put the identificator number
    fill(0, 102, 153, 204); // Fill the rect with color
  
    
    x = x + cell_width;
  }
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  number = number + 60 + 30;
    
  ////////////////////////////////////////////////////////////// left //////////////////////////////////////////////////////////
  x = 0;
  y = 0;

  for (int i=0; i <= 30; i++) { // Creates a loop
    rect(x, y, cell_width, cell_height); // Create a rect
    fill(0); // Fill the text with color
    text(number--, x + cell_width/4, y + cell_height/2); // Put the identificator number
    fill(0, 102, 153, 204); // Fill the rect with color
    
    y = y + cell_height;
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
}
