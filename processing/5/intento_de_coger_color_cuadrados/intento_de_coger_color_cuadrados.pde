import java.awt.*; // Include all the awt libraries

int max_LED_horizontal=20; // Define a int with the max number led horizontal
int max_LED_vertical=10; // Define a int with the max number led vertical

int max_width; // Define an int to get the max width resolution of the user
int max_height; // Define an int to get the max height resolution of the user

int max_rect_horizontal_X; // Define an int for later save data in it
int max_rect_vertical_Y; // Define an int for later save data in it

color[] colors_TOP; // Datatype to store color values
color[] colors_DOWN; // Datatype to store color values 
color[] colors_LEFT; // Datatype to store color values
color[] colors_RIGHT; // Datatype to store color values


PImage screenshot; // Datatype to store the screenshots

void setup() {
  //size(500,500); // Size of the screen
  fullScreen(); // Sets the screen to fullscreen
  frameRate(55000); // Sets the FPS
  
  colors_TOP= new color[20]; // Add a vector to colors_TOP
  colors_DOWN= new color[20]; // Add a vector to colors_DOWN
  colors_LEFT= new color[8]; // Add a vector to colors_LEFT
  colors_RIGHT= new color[8]; // Add a vector to colors_RIGHT
  
  // GET THE RESOLUTION OF THE USER
  GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice(); // To start to capture the resolution of the user
  max_width = gd.getDisplayMode().getWidth(); // Captures the max width and save it in max_width
  max_height = gd.getDisplayMode().getHeight(); // Captures the max height and save it in max_height
  
  max_rect_horizontal_X=max_width/max_LED_horizontal; // Define the size of the rect in the x axis
  max_rect_vertical_Y=max_height/max_LED_vertical; // Define the size of the rect in the y axis
}

void draw() {
  int count_color_LED=0; // Define an int with value 0
  
  // TOP RECTS
  for (int x=1;x<=max_LED_horizontal;x++){ // Starts a loop
    fill(color(random(255), random(255), random(255))); // Fill the rect with random rgb color
    rect((x-1)*max_rect_horizontal_X,0,x*max_rect_horizontal_X,max_rect_vertical_Y); // Creates a rect
  } 
  
  count_color_LED=0; // Reset count_color_LED to 0
  
  // BOTTOM RECTS
  for (int x=1;x<=max_LED_horizontal;x++){ // Starts a loop
    fill(color(random(255), random(255), random(255))); // Fill the rect with random rgb color
    rect((x-1)*max_rect_horizontal_X,(max_height-max_rect_vertical_Y),x*max_rect_horizontal_X,max_rect_vertical_Y); // Creates a rect
  } 
  
  count_color_LED=0; // Reset count_color_LED to 0
  
  // LEFT RECTS
  for (int y=2;y<max_LED_vertical;y++){ // Starts a loop
    count_color_LED++; // Increase the variable count_color_LED
    colors_LEFT[count_color_LED-1]=get(max_width-50,(y-1)*max_rect_vertical_Y+(max_rect_vertical_Y/2)); // Get the color of x position
    rect(0,(y-1)*max_rect_vertical_Y,max_rect_horizontal_X,max_rect_vertical_Y); // Creates a rect
   }
   
   count_color_LED=0; // Reset count_color_LED to 0
   
  // RIGHT RECTS
  for (int y=2;y<max_LED_vertical;y++){ // Starts a loop
    fill(color(random(255), random(255), random(255))); // Fill the rect with random rgb color
    rect((max_width-max_rect_horizontal_X),(y-1)*max_rect_vertical_Y,max_rect_horizontal_X,max_rect_vertical_Y); // Creates a rect
  }
}
