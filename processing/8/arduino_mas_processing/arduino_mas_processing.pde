import processing.serial.*; // Import the library of processing.serial to send data by the serial

Serial myPort;  // Create object from Serial class
String val;     // Data received from the serial port

void setup() {
  myPort = new Serial(this, "/dev/ttyUSB0", 9600); // Define the serial port on arduino is connected
  myPort.bufferUntil('\n'); // Do a linebreak until the buffer
}

void draw() {
  if (mousePressed == true) { // Starts an if
   // If the coniditon is true do:               
   myPort.write('1'); // Sends a 1
   println("1"); // Shows a 1 in the console
  } 
  else { // Starts an else
  // If the condition is false do:
  myPort.write('0'); // Sends a 0
  println("0"); // Shows a 0 in the console
  }   
}
