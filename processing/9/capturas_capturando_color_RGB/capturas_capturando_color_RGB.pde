import java.awt.*; // Include all the awt libraries

PImage screenshot; // Datatype to store the screenshots

int max_LED_X=20; // Define a int with the max number led horizontal
int max_LED_Y=10; // Define a int with the max number led vertical

int max_width; // Define an int to get the max width resolution of the user
int max_height; // Define an int to get the max height resolution of the user

int max_pixel_width; // Define an int to save the width of the rect
int max_pixel_height; // Define an int to save the height of the rect

static final color leds[][] = new color[12][22]; // Define a matrix with color
static final int led_id[][] = new int[12][22]; // Define a matrix with int

void mappear() {
  // Right side
  led_id[10][21] = 0;
  led_id[9][21] = 1;
  led_id[8][21] = 2;
  led_id[7][21] = 3;
  led_id[6][21] = 4;
  led_id[5][21] = 5;
  led_id[4][21] = 6;
  led_id[3][21] = 7;
  led_id[2][21] = 8;
  led_id[1][21] = 9;
  ///////////////////
  // Top side
  led_id[0][20] = 10;
  led_id[0][19] = 11;
  led_id[0][18] = 12;
  led_id[0][17] = 13;
  led_id[0][16] = 14;
  led_id[0][15] = 15;
  led_id[0][14] = 16;
  led_id[0][13] = 17;
  led_id[0][12] = 18;
  led_id[0][11] = 29;
  led_id[0][10] = 20;
  led_id[0][9] = 21;
  led_id[0][8] = 22;
  led_id[0][7] = 23;
  led_id[0][6] = 24;
  led_id[0][5] = 25;
  led_id[0][4] = 26;
  led_id[0][3] = 27;
  led_id[0][2] = 28;
  led_id[0][1] = 29;
  //////////////////
  // Left side
  led_id[1][0] = 30;
  led_id[2][0] = 31;
  led_id[3][0] = 32;
  led_id[4][0] = 33;
  led_id[5][0] = 34;
  led_id[6][0] = 35;
  led_id[7][0] = 36;
  led_id[8][0] = 37;
  led_id[9][0] = 38;
  led_id[10][0] = 39;
  ///////////////////
  // Bottom side
  led_id[11][1] = 40;
  led_id[11][2] = 41;
  led_id[11][3] = 42;
  led_id[11][4] = 43;
  led_id[11][5] = 44;
  led_id[11][6] = 45;
  led_id[11][7] = 46;
  led_id[11][8] = 47;
  led_id[11][9] = 48;
  led_id[11][10] = 49;
  led_id[11][11] = 50;
  led_id[11][12] = 51;
  led_id[11][13] = 52;
  led_id[11][14] = 53;
  led_id[11][15] = 54;
  led_id[11][16] = 55;
  led_id[11][17] = 56;
  led_id[11][18] = 57;
  led_id[11][19] = 58;
  led_id[11][20] = 59;
  //////////////////
}

void getColorTopLine() {
  int top_x = 0;
  int top_y = 5;
  // LEDS Top Side
  for (int i = 1; i <= max_LED_X; i++) { // Starts a loop
    leds[0][i] = get(top_x, top_y); // Get the color of x pixel in x position
    top_x = top_x + max_pixel_width+3; // Actualize the variable top_x with new values
  }
}

void getColorBotLine() {
  int bot_x = 0;
  int bot_y = max_height - 35;
  // LEDS Bottom Side
  for (int i = 1; i <= max_LED_X; i++) { // Starts a loop
    leds[11][i] = get(bot_x, bot_y); // Get the color of x pixel in x position
    bot_x = bot_x + max_pixel_width+3; // Actualize the variable bot_x with new values
  }
}

void getColorLeftLine() {
  int left_x = 5;
  int left_y = 0;
  // LEDS Left Side
  for (int i = 1; i <= max_LED_Y; i++) { // Starts a loop
    leds[i][0] = get(left_x, left_y); // Get the color of x pixel in x position
    left_y = left_y + max_pixel_height+5; // Actualize the variable left_y with new values
  }
}

void getColorRightLine() {
  int right_x = max_width - 5;
  int right_y = 0;
  // LEDS Right Side
  for (int i = 1; i <= max_LED_Y; i++) { // Starts a loop
    leds[i][21] = get(right_x, right_y); // Get the color of x pixel in x position
    right_y = right_y + max_pixel_height+5; // Actualize the variable right_y with new values
  }
}

void printMatrix() {
   for (int i=0; i<leds.length; i++) { // Starts a loop
     for (int j=0; j<leds[0].length; j++) { // Starts a loop inside a loop
       print(leds[i][j]+" ");   // Print the position of the leds
     }
     print("\n"); // Linebreak
   } 

}
void printRGB() {
   for (int i=0; i<leds.length; i++) { // Starts a loop
     for (int j=0; j<leds[0].length; j++) { // Starts a loop inside a loop
          color color_pixel = leds[i][j]; // Save the color of the leds
          float r,g,b; // Starts the variables r,g,b
          r = red(color_pixel); // Define r in the color_pixel
          g = green(color_pixel); // Define g in the color_pixel
          b = blue(color_pixel); // Define b in the color_pixel
          print("("+r+","+g+","+b+")"); // Shows the rgb values recorded
     }
     print("\n"); // Linebreak
   } 
}

void setup() {
  // GET THE RESOLUTION OF THE USER
  GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice(); // To start to capture the resolution of the user
  max_width = gd.getDisplayMode().getWidth(); // Captures the max width and save it in max_width
  max_height = gd.getDisplayMode().getHeight(); // Captures the max height and save it in max_height
  //size(500,500); // Size of the screen
  fullScreen(); // Sets the screen to fullscreen
  max_pixel_width  = max_width/22; // Divide the max width of the screen by the max horizontal number of leds + 2
  max_pixel_height = max_height/12; // Divide the max height of the screen by the max height number of leds + 2
};

void draw() {
  if (frameCount % 0.5 == 0) screenshot(); // Time between screenshots
  if (screenshot != null) image(screenshot, 0, 0, width, height); // Define the size of the screenshots
  getColorTopLine(); // Calls the void to execute it continually
  getColorBotLine(); // Calls the void to execute it continually
  getColorLeftLine(); // Calls the void to execute it continually
  getColorRightLine(); // Calls the void to execute it continually
  //printMatrix(); // Calls the void to execute it continually
  printRGB(); // Calls the void to execute it continually
}

void screenshot() {
  try {
    screenshot = new PImage(new Robot().createScreenCapture(new Rectangle(0, 0, displayWidth, displayHeight))); // Do and show the screenshots in a rectangle
  } catch (AWTException e) {} // That function tries to handle the exceptions
}
